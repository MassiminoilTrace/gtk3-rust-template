// Copyright © 2022 Massimo Gismondi

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>


use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};
use gtk::{Box, Orientation};
use gtk::builders::{LabelBuilder};
use gtk::gdk_pixbuf::Pixbuf;
use gio;


fn main() {
    gio::resources_register_include!("compiled.gresource").unwrap();

    let app = Application::builder()
        .application_id("org.example.HelloWorld")
        .build();

    app.connect_activate(|app| {
        let win = ApplicationWindow::builder()
            .application(app)
            .default_width(320)
            .default_height(200)
            .title("Hello, World!")
            .icon(
                    &Pixbuf::from_resource(
                    "/it/yourcompany/helloworld/icon.png"
                ).unwrap()
            )
            .build();

        let cont = Box::new(Orientation::Vertical, 5);
        win.add(&cont);

        cont.add(
            &LabelBuilder::new().label("Hello world!").build()
        );

        win.show_all();
    });

    app.run();
}