# The goal
Are you exasperated too by the bloatware and boilerplate of meson/virtualization/flatpak/snap/gnome builder/ninja?
Do you also prefer "less bloat" approach compared to the "automating bloat generation" of these systems?

You just want a barebone GTK application that is simply compiled with `cargo` as every other rust program? Maybe you're even trying to publish it in a standard `.deb` package?

That's what I'm trying to achieve in this repository, feel free to use this template as you wish, as long as you check and make sure to comply with the license.

# How
A lean template for a GTK3 application.

This includes a `build.rs` script that compiles static resources through the `gio` crate.
It comes with a sample icon you can customize.

The icon is set as an application icon automatically so it appears in the task bar of your computer.


# Compiling
Make sure you have installed the `libgtk-3-dev` libraries. This can be named a little different depending on your distribution.

If you're compiling for Windows, make sure to install the GTK3 dev libraries inside the `mingw-w64` environment.

## GNU/Linux
As simple as:
`cargo run`
or
`cargo run --release`

You can even package it as a debian standard `.deb` package by using this [other repository](https://gitlab.com/MassiminoilTrace/rust-debian-packaging).

## Windows
```
sudo apt install mingw-w64
rustup target add x86_64-pc-windows-gnu
rustup toolchain install stable-x86_64-pc-windows-gnu

cargo build --target x86_64-pc-windows-gnu
```

If you're planning to distribute the executable for Windows, make sure to include the necessary `.dll` as noted [here](https://github.com/gtk-rs/gtk/issues/422#issuecomment-270259393).

As Windows doesn't come with any package manager, you may want to use [NSIS](https://nsis.sourceforge.io/Main_Page) to make a graphical installer to place the needed files in the correct place.

By default, gtk programs will spawn a terminal too. You can hide the terminal by following [this tutorial](https://stackoverflow.com/questions/29763647/how-to-make-a-program-that-does-not-display-the-console-window/29764309#29764309).

## Mac OS
I don't have any Mac OS computer to try it. I've no idea on how Mac programs should be packaged and I've zero interest in trying it. Just switch to a Gnu/Linux operating system, or contribute to this repository with additional instructions instead.

# License and Copyright
Copyright © 2022 Massimo Gismondi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/